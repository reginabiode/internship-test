# Avaliação prática - Programa de desenvolvimento de talentos


## Instruções

1. Faça um fork do repositório (Como faço isso? [Passo 1](https://bytebucket.org/everestti/software-engineer-test/raw/852b88d2ba088ff3cc5b4ecbbedfe1a6a403fb59/assets/fork2.JPG) , [Passo 2](https://bytebucket.org/everestti/software-engineer-test/raw/852b88d2ba088ff3cc5b4ecbbedfe1a6a403fb59/assets/fork1.JPG) ), [Não tenho o GIT instalado](https://www.youtube.com/watch?v=TF7Vvk0s86g), [Nunca utilizei o GIT](https://try.github.io/levels/1/challenges/1)
2. Faça um clone do fork para sua máquina
3. Crie um projeto ASP.NET, onde a página principal funciona como um índice para a resolução dos problemas propostos.
4. Finalizada a resolução do projeto, adicione ao repositório os usuários cbfranca (Cleidson França) e lopes19 (Mateus Lopes) para que façamos a correção. ([Como faço isso?](https://bytebucket.org/everestti/software-engineer-test/raw/b811f9c39cbb9bf60a35f79da253fe84382b16b8/assets/invite.JPG))
5. Agora é só aguardar o nosso contato!


Observações

1. Não é obrigatória a estilização do projeto com CSS, porém será considerado um _diferencial_.
2. Caso seja familiarizado com ASP.NET/C#, fique à vontade para utilizá-lo no projeto.


## Problema 1

Uma rei requisitou os serviços de um operário e disse-lhe que pagaria qualquer preço. O operário, que passava necessidades e previsava de alimentos , perguntou ao rei se o pagamento poderia ser feito com grãos de trigo dispostos em um tabuleiro de xadrez, de tal forma que o primeiro quadro deveria conter apenas um grão e os quadros subseqüentes , o dobro do quadro 
anterior. O rei achou o trabalho barato e pediu que o serviço fosse executado, sem se dar conta de que seria impossível efetuar o pagamento. Faça um algoritmo para calcular o número de grãos que o rei esperava receber.

## Problema 2    

A partir do schema dado e sabendo-se que as chaves primárias estão em negrito, as chaves estrangeiras estão representadas em itálico e cada chave estrangeira tem o mesmo nome da chave primária a qual ela se referencia (ie: codProd de ItemPedido se referencia a codProd de Produto), responda as questões abaixo utilizando SQL:

* Cliente ( __codCli__, nomeCli, cidadeCli )
* Pedido ( __codPed__, dtPed, cidadeEntrega, codCli )
* Produto ( __codProd__, descrProd, precoUnitProd )
* Fornecedor( __codForn__, nomeForn, cidadeForn )
* ItemPedido (__ _codPed_, _codProd_, _codForn_ __, qtdePedida )


a) Listar o nome dos clientes que receberam algum produto de algum fornecedor localizado na mesma cidade do cliente.

b) Listar o nome dos Clientes que receberam produtos dos fornecedores ‘Acme’ ou ‘Ajax’ e que cujo preço unitário está abaixo do preço de todos os produtos já comprados pelo cliente ‘Bompreço’. 


Obs: Para essa questão basta exibir a query (consulta) na tela como uma string.


## Problema 3

Um grupo de empreendedores resolvem projetar um celuar para pessoas com deficiência visual. Você foi contratado para desenvolver esse projeto. Escreva um breve texto contando para os empreendedores que lhe contrataram o que você tem em mente para tal projeto. Como será esse celular, como ele funcionaria, quais seriam as suas características, etc. 

## Problema 4

Como você explicaria o que é um Banco de dados para seu sobrinho de 8 anos?


Sucesso!